# Pointeur vers les resspources de formation

La documenation générée est disponible sur le site [https://ebraux.gitlab.io/](https://ebraux.gitlab.io/)

## Tests en local du site

Build de l'image
```bash
docker build -t mkdocs_ebraux.gitlab.io .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_ebraux.gitlab.io mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_ebraux.gitlab.io mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_ebraux.gitlab.io rm -rf /work/site
docker image rm mkdocs_intro-cloud
```
Rem : Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_ebraux.gitlab.io mkdocs new ebraux.gitlab.io
sudo chown -R $(id -u -n):$(id -g -n)  *
```
