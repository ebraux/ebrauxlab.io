# Terraform

---

## Les modules de cours :

- Introduction à Terraform :  [https://ebraux.gitlab.io/terraform-101/](https://ebraux.gitlab.io/terraform-101/)

---

## Labs

- Labs de prise en main d'Terraform : [https://ebraux.gitlab.io/terraform-labs/](https://ebraux.gitlab.io/terraform-labs/)
    


![image alt <>](img/terraform.png)

