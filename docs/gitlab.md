# Gitlab

---

## Les modules de cours :

- Gitlab 101 - Introduction à Gitlab CI :  [https://ebraux.gitlab.io/gitlab-ci-101/](https://ebraux.gitlab.io/gitlab-ci-101/)

---

## Labs

- Labs Gitlab CI : [https://ebraux.gitlab.io/gitlab-ci-labs/](https://ebraux.gitlab.io/gitlab-ci-labs/)
    


![image alt <>](img/gitlab.png)

