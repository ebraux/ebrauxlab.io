# Linux

---

## Les modules de cours :

- Introduction à Linux :  [https://ebraux.gitlab.io/linux-101/](https://ebraux.gitlab.io/linux-101/)
- Administration  Linux :  [https://ebraux.gitlab.io/linux-102/](https://ebraux.gitlab.io/linux-102/)
- 
---

## Labs

- [https://ebraux.gitlab.io/linux-labs/](https://ebraux.gitlab.io/linux-labs/)


![image alt <>](img/linux.png)

