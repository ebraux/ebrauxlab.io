# Openstack

---

## Les modules de cours :

- Introduction à Openstack :  [https://ebraux.gitlab.io/openstack-101/](https://ebraux.gitlab.io/openstack-101/)
- Inside Openstack - Architecture et installation : [https://ebraux.gitlab.io/openstack-inside/](https://ebraux.gitlab.io/openstack-inside/)
- Utilisation de l'Openstack Cli: [https://ebraux.gitlab.io/openstack-cli/](https://ebraux.gitlab.io/openstack-cli/)
- Introduction à l'utilisation de l'orchestrateur Heat [https://ebraux.gitlab.io/openstack-heat/](https://ebraux.gitlab.io/openstack-heat/)


---

## Labs

Les labs autour d'Openstack illustrant ces sessions sont disponibles : [https://ebraux.gitlab.io/openstack-labs/](https://ebraux.gitlab.io/openstack-labs/)

---

![image alt <>](img/openstack.png)

