# Ansible

---

## Les modules de cours :

- Introduction à Ansible :  [https://ebraux.gitlab.io/ansible-101/](https://ebraux.gitlab.io/ansible-101/)

---

## Labs

- Labs de prise en main d'Ansible : [https://ebraux.gitlab.io/ansible-labs/](https://ebraux.gitlab.io/ansible-labs/)
    


![image alt <>](img/ansible.png)

