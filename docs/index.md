# Ressources de formation


---

Pour me contacter : -ebraux@gmail.com-

---
- Les contenus disponibles sont sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
- Selon les options : 
    - Attribution
    - Pas d’Utilisation Commerciale
    - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](assets/cc-by-nc-sa.png)

---
